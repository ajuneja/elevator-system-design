class Request {
    constructor(from,destination,numPeople) {
        this.from = from
        this.destination = destination
        this.numPeople = numPeople
    }
    getFrom(from) {
        return this.from
    }

    getTo(to) {
       return this.to
    }

    setFrom(from) {
        this.from = from
    }
    
    setTo(to) {
        this.to = to
    }

    getNumPeople() {
        return this.numPeople
    }

    setNumPeople(numPeople) {
        this.numPeople = numPeople
    }
}

module.exports = Request