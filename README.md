# Notes, Assumptions, and Objectives
  - System design coding challenge on designing an elevator system. JavaScript requirement.
  - The system must decouples an elevator's movement from the policy/business rules written for this particular assignment. In the future we may want different subsystems(ex: 2 of the elevators are reserved for executives and follow a different policy like always wait at the top executive floors) while the core elevator functions (move up/down etc.) remain the same.
  - The system must be "atomically processed" (i.e no two elevators process the same request) and must arrive at a consensus quickly
  - When an elevator reports its current state it must 'freeze' till a consensus is reached on which elevator will handle the new request. Otherwise the system will reach an inconsistant state in that it will make a decision based on state that no longer exists. 
  - The system must balance the frequency it freezes the elevators to process new requests so that current requests don't have high latency but freeze them often enough that new requests don't suffer from starvation
  - The time required to make a decision should be minimized 

## Peer to Peer Approach

  - Each elevator communicates with every other elevator to reach a consensus
  - No single point of failure, a big positive. If one elevator goes down all the other elevators still work
  - Does not scale very well - the chatter between the elevators increases at a rate of around n^2 messages. For a small amount of elevators (2-4)+ its ok, but 10 it would be around 100+ messages to reach a consensus
  - The time to reach a consensus is much higher (vioates objective #5)

## Centralized Approach [Chosen approach]
- Lower latency and much higher scalability due to less communication (elevators respond to one central node instead of every other elevator)
-If only one controller - there is a single point of failure
-If you cluster the controllers such that there are many instances distributed across more then 1 machine you eliminate the single pt of failure. 
- In a real deployment the controllers wouldn't store the queue of pending requests - we would use an external system to keep state (ex: apache zookeeper) so that the controllers themselves are stateless and don't need any logic to synchronize. 
- If one dies, another controller takes over - the only job of the controller is polling the queue and executing the policy to assign it to an elevator. The queue/list of requests in the ElevatorController should be the address of some distributed store like apache zookeeper, mq etc. 

## Key Classes
- Request - contains information relating to number of people, floor to be picked up from & dropped off at
- Policy - contains business logic for routing a specific request to a specific elevator (given information on the state of all current elevators)
- Elevator - Contains state relating to its current floor, current direction, number of people
- ElevatorController - Responsible for periodically polling a queue of requests, freezing elevators and executing a policy to match an elevator to a given request
- ElevatorSystem - An interface/entry point that the UI on each floor interacts with for pushing new requests and generating success/error messages.
- ElevatorUI/Client - The client that takes input from the user, pushes it to the system, and responds to error/success messages
- ElevatorFactory - Encapsulates the logic for instantiating an elevator system
        
        

## New Request Path
- User enters floor number and number of people by pressing a button on a screen.
- The screen turns into an hourglass shape indicating it is processing
- A 'request' object is created detailing the current floor,destination,the number of people, a unique requestid and is sent to the elevator system
--    i) The active controller writes the request to its internal queue of requests, updates all other controllers and sends an ack message back to the UI
--            ii) If an ack message is not recieved, the UI tries another controller and waits for an ack message. In the case that both writes were successful despite a timed out/network issue on the first write, the controllers will recognize the duplicate uuid and avoid writing the request twice

##  Processing a request
- Each elevator contains an array of dropoff and pickup requests. The elevator analyzes its current direction, current floor and determines its next stop depending on if the next pickup request is closer or the next drop off request is closer
- Each elevator goes directly to its next stop without stopping at each floor to increase throughput. If both pickup and dropoff requests are empty it will go floor to floor.
- The elevator controller periodically 'freezes' the elevators and executes a policy to determine which elevator should process a new request. The frequency in which it freezes is proportional to the number of pending requests in the queue. 
- sleepTime = (1/(requestQueue.length + 1)*k) + c ;where k and c are constants
                where sleep time is the amount of time between processing requests/freezing state
-  The policy pushes the request to the corresponding elevator
- The controller unfreezes the elevators and sleeps for sleepTime
    

## Didn't finish/TODO:
- Add condition for elevator to check the number of trips/go into service mode
- Controller has to filter out elevators not in servince in service
- Finish the ElevatorFactory which creates the elevators, controllers, injects policy etc.
- Unit & integration tests
- A cmd line interface/UI that takes input from a user and uses the elevator system as a backend (they will be decoupled)    
